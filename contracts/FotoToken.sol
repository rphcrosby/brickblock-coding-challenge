pragma solidity ^0.4.23;

import "./interfaces/IERC20.sol";
import "../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol";


contract FotoToken is IERC20 {
  event Mint(address indexed to, uint value);

  mapping (address => uint256) public balances;
  mapping (address => mapping (address => uint256)) internal allowed;

  address public owner_; // Ownable
  string private name_; // ERC20 name
  string private symbol_; // ERC20 symbol
  uint8 private decimals_; // ERC20 decimals
  uint256 public totalSupply_; // ERC20 total supply

  constructor(
    uint256 _initialAmount,
    string _tokenName,
    uint8 _decimalUnits,
    string _tokenSymbol
  ) public {
    balances[msg.sender] = _initialAmount;
    totalSupply_ = _initialAmount;
    name_ = _tokenName;
    decimals_ = _decimalUnits;
    symbol_ = _tokenSymbol;
    owner_ = msg.sender;
  }

  modifier onlyOwner() {
    require(msg.sender == owner_, "Only the owner can do that");
    _;
  }

  function owner() public view returns (address) {
    return owner_;
  }

  function name() public view returns (string) {
    return name_;
  }

  function symbol() public view returns (string) {
    return symbol_;
  }

  function decimals() public view returns (uint8) {
    return decimals_;
  }

  function totalSupply() public view returns (uint256) {
    return totalSupply_;
  }

  function balanceOf(address _owner) public view returns (uint256) {
    return balances[_owner];
  }

  function transfer(address _to, uint256 _value) public returns (bool success) {
    require(_to != address(0), "Cannot transfer to address 0");
    require(_value <= balances[msg.sender], "You don't have enough FOTO for this transfer");
    balances[msg.sender] = SafeMath.sub(balances[msg.sender], _value);
    balances[_to] = SafeMath.add(balances[_to], _value);
    emit Transfer(msg.sender, _to, _value);
    return true;
  }

  function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
    require(_to != address(0), "Cannot transfer to address 0");
    require(_value <= balances[_from], "Not enough FOTO in source account for this transfer");
    require(_value <= allowed[_from][_to], "Not enough FOTO approved");

    balances[_from] = SafeMath.sub(balances[_from], _value);
    balances[_to] = SafeMath.add(balances[_to], _value);
    allowed[_from][_to] = SafeMath.sub(allowed[_from][_to], _value);
    emit Transfer(_from, _to, _value);
    return true;
  }

  function approve(address _spender, uint256 _value) public returns (bool success) {
    allowed[msg.sender][_spender] = _value;
    emit Approval(msg.sender, _spender, _value);
    return true;
  }

  function allowance(address _owner, address _spender) public view returns (uint256 remaining) {
    return allowed[_owner][_spender];
  }

  function mint(address _to, uint _amount) public onlyOwner returns (bool) {
    totalSupply_ = SafeMath.add(totalSupply_, _amount);
    balances[_to] = SafeMath.add(balances[_to], _amount);
    emit Mint(_to, _amount);
    return true;
  }
}
