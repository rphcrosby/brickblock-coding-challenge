const assert = require('assert')
const FotoToken = artifacts.require('FotoToken')

describe('when deploying', () => {
  contract('FotoToken', accounts => {
    const account1 = accounts[0]
    const account2 = accounts[1]
    let token

    before('setup', async () => {
      token = await FotoToken.new(1000, 'FotoToken', 8, 'FOTO')
    })

    it('should be constructed correctly', async () => {
      const tokenName = await token.name()
      const tokenTotalSupply = await token.totalSupply()
      const tokenDecimals = await token.decimals()
      const tokenSymbol = await token.symbol()
      const tokenOwner = await token.owner()

      assert.equal(tokenName, 'FotoToken', 'The name should match')
      assert.equal(tokenTotalSupply, 1000, 'The total supply should match')
      assert.equal(tokenDecimals, 8, 'The decimals should match')
      assert.equal(tokenSymbol, 'FOTO', 'The symbol should match')
      assert.equal(tokenOwner, account1, 'The symbol is not owned by the owner')
    })

    it('should transfer', async () => {
      const transferAmount = 2
      const account1BalanceBefore = await token.balanceOf(account1)
      const account2BalanceBefore = await token.balanceOf(account2)

      await token.transfer(account2, transferAmount)

      const account1BalanceAfter = await token.balanceOf(account1)
      const account2BalanceAfter = await token.balanceOf(account2)

      assert.equal(
        account1BalanceBefore.minus(account1BalanceAfter),
        transferAmount,
        'The balance for account 1 has not been decremented by the transfer amount'
      )

      assert.equal(
        account2BalanceAfter.minus(account2BalanceBefore),
        transferAmount,
        'The balance for account 2 has not been incremented by the transfer amount'
      )
    })

    it('should approve', async () => {
      const allowanceAmount = 2
      const account2AllowanceBefore = await token.allowance(account1, account2)

      await token.approve(account2, allowanceAmount)

      const account2AllowanceAfter = await token.allowance(account1, account2)

      assert.equal(
        account2AllowanceAfter.minus(account2AllowanceBefore),
        allowanceAmount,
        'The allowance for account 2 has not been incremented by the allowance amount'
      )
    })

    it('should transfer from', async () => {
      const transferAmount = 2
      const account1BalanceBefore = await token.balanceOf(account1)
      const account2BalanceBefore = await token.balanceOf(account2)
      const account2AllowanceBefore = await token.allowance(account1, account2)

      await token.transferFrom(account1, account2, transferAmount)

      const account1BalanceAfter = await token.balanceOf(account1)
      const account2BalanceAfter = await token.balanceOf(account2)
      const account2AllowanceAfter = await token.allowance(account1, account2)

      assert.equal(
        account1BalanceBefore.minus(account1BalanceAfter),
        transferAmount,
        'The balance for account 1 has not been decremented by the transfer amount'
      )

      assert.equal(
        account2BalanceAfter.minus(account2BalanceBefore),
        transferAmount,
        'The balance for account 2 has not been incremented by the transfer amount'
      )

      assert.equal(
        account2AllowanceBefore.minus(account2AllowanceAfter),
        transferAmount,
        'The allowance for account 2 has not been decremented by the transfer amount'
      )
    })

    it('should mint new tokens', async () => {
      const mintAmount = 2
      const totalSupplyBefore = await token.totalSupply()
      const account2BalanceBefore = await token.balanceOf(account2)

      await token.mint(account2, mintAmount)

      const totalSupplyAfter = await token.totalSupply()
      const account2BalanceAfter = await token.balanceOf(account2)

      assert.equal(
        account2BalanceAfter.minus(account2BalanceBefore),
        mintAmount,
        'The balance for account 2 has not been incremented by the mint amount'
      )

      assert.equal(
        totalSupplyAfter.minus(totalSupplyBefore),
        mintAmount,
        'The total supply of FOTO has not been incremented by the mint amount'
      )
    })

    it('should not be able to transfer to account 0', async () => {
      try {
        await token.transfer('0x0', 2)
        assert(false, 'Should not be able to transfer to address 0')
      } catch (error) {
        assert(
          /Cannot transfer to address 0/.test(error.message || error),
          'The error message should contain the "revert"'
        )
      }
    })

    it('should not be able to transfer more than available', async () => {
      try {
        await token.transfer(account2, 2000)
        assert(false, 'Should not be able to transfer more than available')
      } catch (error) {
        assert(
          /You don't have enough FOTO/.test(error.message || error),
          'The error message should contain the "revert"'
        )
      }
    })

    it('should not be able to transferFrom to account 0', async () => {
      try {
        await token.transferFrom(account1, '0x0', 2)
        assert(false, 'Should not be able to transferFrom to address 0')
      } catch (error) {
        assert(
          /Cannot transfer to address 0/.test(error.message || error),
          'The error message should contain the "revert"'
        )
      }
    })

    it('should not be able to transferFrom more than available', async () => {
      try {
        await token.transferFrom(account1, account2, 2000)
        assert(false, 'Should not be able to transferFrom to address 0')
      } catch (error) {
        assert(
          /Not enough FOTO in source account/.test(error.message || error),
          'The error message should contain the "revert"'
        )
      }
    })

    it('should not be able to transferFrom more than available', async () => {
      try {
        await token.transferFrom(account1, account2, 20)
        assert(false, 'Should not be able to transferFrom to address 0')
      } catch (error) {
        assert(
          /Not enough FOTO approved/.test(error.message || error),
          'The error message should contain the "revert"'
        )
      }
    })
  })
})
