'use strict'

import React from 'react'
import { render } from 'react-dom'
import { Drizzle, generateStore } from 'drizzle'
import { DrizzleContext } from 'drizzle-react'
import App from './components/App'
import FotoToken from './../build/contracts/FotoToken.json'

const options = {
  web3: {
    block: false,
    fallback: {
      type: 'ws',
      url: 'ws://127.0.0.1:8545'
    }
  },
  contracts: [FotoToken],
  polls: {
    accounts: 1500
  }
}
const store = generateStore(options)
const drizzle = new Drizzle(options, store)

class Foto extends React.Component {
  render() {
    return (
      <DrizzleContext.Provider drizzle={drizzle}>
        <App />
      </DrizzleContext.Provider>
    )
  }
}

render(<Foto />, document.querySelector('#app'))
