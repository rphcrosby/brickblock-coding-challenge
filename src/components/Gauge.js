import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

const styles = () => ({
  outer: {
    display: 'flex'
  },
  inner: {
    margin: 'auto'
  },
  circleOuter: {
    borderRadius: '50%',
    width: '300px',
    height: '300px',
    background: 'linear-gradient(225deg, #ed7768 0%,#b44e86 50%,#30497f 80%)',
    border: '20px solid rgb(43,26,90)',
    position: 'relative',
    marginBottom: '25px'
  },
  circleInner: {
    borderRadius: '50%',
    width: '236px',
    height: '236px',
    background: 'rgb(22,18,62)',
    border: '10px solid rgb(49,29,114)',
    position: 'absolute',
    top: '12px',
    left: '12px'
  },
  title: {
    fontFamily: 'Chivo,sans-serif',
    fontWeight: 'bold',
    letterSpacing: '1px',
    color: 'rgb(109,109,145)',
    fontSize: '16px',
    width: '100%',
    marginTop: '40px',
    textAlign: 'center',
    display: 'block'
  },
  amount: {
    fontFamily: 'Chivo,sans-serif',
    fontWeight: 'bold',
    letterSpacing: '1px',
    color: 'rgb(232,233,235)',
    fontSize: '24px',
    width: '100%',
    marginTop: '35px',
    textAlign: 'center',
    display: 'block'
  }
})

class Gauge extends Component {
  render() {
    return (
      <div className={this.props.classes.outer}>
        <div className={this.props.classes.inner}>
          <div className={this.props.classes.circleOuter}>
            <div className={this.props.classes.circleInner}>
              <span className={this.props.classes.title}>
                {this.props.title}
              </span>
              <span className={this.props.classes.amount}>
                {this.props.amount}
              </span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Gauge.propTypes = {
  classes: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  amount: PropTypes.string.isRequired
}

export default withStyles(styles)(Gauge)
