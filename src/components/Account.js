import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

const styles = () => ({
  subtitle: {
    fontFamily: 'Chivo,sans-serif',
    fontWeight: 'bold',
    fontSize: '14px',
    letterSpacing: '1px',
    color: 'rgba(255,255,255,0.3)',
    paddingBottom: '25px',
    borderBottom: '1px solid rgb(42,28,79)',
    marginBottom: '25px'
  }
})

class Account extends Component {
  render() {
    const { state } = this.props
    const account = state.accounts[0]
    return (
      <Typography
        variant="title"
        className={this.props.classes.subtitle}
        align="center"
      >
        {account}
      </Typography>
    )
  }
}

Account.propTypes = {
  classes: PropTypes.object.isRequired,
  state: PropTypes.object.isRequired
}

export default withStyles(styles)(Account)
