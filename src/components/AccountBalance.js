import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Gauge from './Gauge'

class AccountBalance extends Component {
  state = {
    key: null
  }

  componentDidMount() {
    const { drizzle, state } = this.props
    const key = drizzle.contracts.FotoToken.methods['balanceOf'].cacheCall(
      state.accounts[0]
    )
    this.setState({ key })
  }

  render() {
    const { state } = this.props
    const balance = state.contracts.FotoToken.balanceOf[this.state.key]
    const amount = (balance && balance.value + ' FOTO') || '...'
    return <Gauge title="Total" amount={amount} />
  }
}

AccountBalance.propTypes = {
  drizzle: PropTypes.object.isRequired,
  state: PropTypes.object.isRequired
}

export default AccountBalance
