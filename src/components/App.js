import React from 'react'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Grid from '@material-ui/core/Grid'
import CssBaseline from '@material-ui/core/CssBaseline'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import AccountBalanceContainer from './../containers/AccountBalanceContainer'
import AccountContainer from './../containers/AccountContainer'
import TransferContainer from './../containers/TransferContainer'

const customTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#b44e86'
    },
    type: 'dark'
  }
})

const styles = theme => ({
  background: {
    backgroundColor: 'rgb(62,32,98)',
    minHeight: '100vh'
  },
  card: {
    backgroundColor: 'rgb(40,24,77)',
    borderRadius: '10px'
  },
  cardContent: {
    backgroundColor: 'rgb(40,24,77)',
    borderRadius: '10px',
    padding: theme.spacing.unit * 4
  },
  title: {
    fontFamily: 'Chivo,sans-serif',
    fontWeight: 'bold',
    fontSize: '16px',
    letterSpacing: '1px',
    color: 'white',
    marginBottom: '25px'
  }
})

class Foto extends React.Component {
  render() {
    return (
      <CssBaseline>
        <MuiThemeProvider theme={customTheme}>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            className={this.props.classes.background}
          >
            <Grid item xs={4}>
              <Card className={this.props.classes.card}>
                <CardContent className={this.props.classes.cardContent}>
                  <Typography
                    variant="title"
                    className={this.props.classes.title}
                    align="center"
                  >
                    BrickBlock.io Coding Challenge
                  </Typography>
                  <AccountContainer />
                  <AccountBalanceContainer />
                  <TransferContainer />
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </MuiThemeProvider>
      </CssBaseline>
    )
  }
}

Foto.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Foto)
