import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

const styles = theme => ({
  button: {
    marginTop: '15px',
    height: '56px'
  },
  input: {
    color: 'white',
    marginRight: theme.spacing.unit
  },
  title: {
    fontFamily: 'Chivo,sans-serif',
    fontWeight: 'bold',
    fontSize: '16px',
    letterSpacing: '1px',
    color: 'white',
    marginBottom: '25px'
  }
})

class Transfer extends Component {
  state = {
    to: '',
    amount: 0
  }

  transferFoto() {
    const { drizzle } = this.props
    drizzle.contracts.FotoToken.methods.transfer.cacheSend(
      this.state.to,
      this.state.amount
    )
    this.setState({ to: '', amount: 0 })
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value })
  }

  render() {
    return (
      <div>
        <Typography variant="title" className={this.props.classes.title}>
          Transfer
        </Typography>
        <TextField
          id="outlined-name"
          label="Send to"
          value={this.state.to}
          onChange={this.handleChange('to')}
          InputLabelProps={{
            shrink: true
          }}
          InputProps={{
            className: this.props.classes.input
          }}
          margin="normal"
          variant="filled"
          fullWidth={true}
        />
        <TextField
          id="outlined-number"
          label="Amount"
          value={this.state.amount}
          onChange={this.handleChange('amount')}
          type="number"
          InputProps={{
            className: this.props.classes.input
          }}
          inputProps={{ min: '0' }}
          InputLabelProps={{
            shrink: true
          }}
          margin="normal"
          variant="filled"
          fullWidth={true}
        />
        <Button
          variant="contained"
          color="primary"
          onClick={() => this.transferFoto()}
          size="large"
          className={this.props.classes.button}
          fullWidth={true}
        >
          Transfer
        </Button>
      </div>
    )
  }
}

Transfer.propTypes = {
  drizzle: PropTypes.object.isRequired,
  state: PropTypes.object.isRequired
}

export default withStyles(styles)(Transfer)
