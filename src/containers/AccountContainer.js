import React, { Component } from 'react'
import { DrizzleContext } from 'drizzle-react'
import Account from './../components/Account'
import LinearProgress from '@material-ui/core/LinearProgress'

class AccountContainer extends Component {
  render() {
    return (
      <DrizzleContext.Consumer>
        {drizzleContext => {
          const { drizzle, drizzleState, initialized } = drizzleContext
          if (!initialized) {
            return <LinearProgress />
          }

          return <Account drizzle={drizzle} state={drizzleState} />
        }}
      </DrizzleContext.Consumer>
    )
  }
}

export default AccountContainer
