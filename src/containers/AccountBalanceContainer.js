import React, { Component } from 'react'
import { DrizzleContext } from 'drizzle-react'
import AccountBalance from './../components/AccountBalance'

class AccountBalanceContainer extends Component {
  render() {
    return (
      <DrizzleContext.Consumer>
        {drizzleContext => {
          const { drizzle, drizzleState, initialized } = drizzleContext
          if (initialized) {
            return <AccountBalance drizzle={drizzle} state={drizzleState} />
          }
        }}
      </DrizzleContext.Consumer>
    )
  }
}

export default AccountBalanceContainer
