import React, { Component } from 'react'
import { DrizzleContext } from 'drizzle-react'
import Transfer from './../components/Transfer'

class TransferContainer extends Component {
  render() {
    return (
      <DrizzleContext.Consumer>
        {drizzleContext => {
          const { drizzle, drizzleState, initialized } = drizzleContext
          if (initialized) {
            return <Transfer drizzle={drizzle} state={drizzleState} />
          }
        }}
      </DrizzleContext.Consumer>
    )
  }
}

export default TransferContainer
