# BrickBlock.io Coding Challenge

Here is my submission for the [BrickBlock.io Coding Challenge](https://github.com/brickblock-io/coding-challenge-blockchain). This document aims to outline the general architecture of the solution and explain my design choices.

## Core Concept

The general concept behind this submission is the introduction of a new ERC20 token named "FotoToken" (Symbol "FOTO") which allows photographers to tokenize their photos and therefore sell them.

The additional functionality added comes in the form of ownership and minting.

## Prerequisites

- [Node.js](https://nodejs.org/en/) Latest LTS release
- [yarn](https://yarnpkg.com/)

## Installing

To install all dependencies just run `yarn`. Then copy the `.env.example` file to `.env`.

To build everything run `yarn build`.

## Testing

All unit tests can be run using `yarn test`. Truffle is responsible for testing Solidity code.

## Linting

Linting everything is done with `yarn lint`. ESLint and Prettier are responsible for linting JS files and Solium is used to
lint Solidity files.

## Deployment

### Local Testnet

1. First the local blockchain should be started with `yarn start:dev`
2. Then build & deploy contracts using `yarn build && yarn migrate:dev`
3. We can then interact with the deployed contracts using `yarn repl:dev` which will launch a Truffle console

### Ropsten

1. Deploying contracts to ropsten can be done using `yarn migrate:ropsten`

## dApp

The dApp can be started using `yarn start:dapp` which should start a local server available at `http://localhost:8080`.

The dApp can be deployed using `yarn deploy:dapp` which deploys to a firebase app.

_Note: You need access to my firebase account for this deployment to work :)_

The deployed dApp is available at https://brickblock-38069.firebaseapp.com.

The dApp is built using React and Drizzle. I chose not to use Redux mostly so I could play around with the recently added
React Contexts :)

## Libraries

- **Babel** - Ensured I could use some next-gen Javascript in my dApp code.

- **Truffle** - Framework for building, testing and deploying contracts amongst other things

- **Drizzle** - Great wrapper around web3 calls and integration with react too

- **Ganache** - For running a local blockchain for development and tests

- **ESLint + Prettier + Solium** - Ensured that my code was styled consistently + other benefits like security best practices

- **OpenZepplin Solidity** - Mostly there to use their SafeMath library

- **React** - For building the dApp

## Further Improvements

There are some things I'd have liked to have done, but chose not to simply because of time. I hope by listing them here, you'll forgive me for not going too overboard.

- Splitting up `FotoToken.sol` - Extra functionality such as ownership and minting could be split into separate contracts e.g. `MintableToken` and `Ownable`.

- Better handling in dApp when unable to connect - It currently just continues to load. Instead it should show a better error.

- Better error handling for the transfer form - Some error cases like a missing address, or invalid amount (e.g. not enough balance) could be shown here.
