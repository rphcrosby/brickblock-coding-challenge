const FotoToken = artifacts.require('./FotoToken.sol')

module.exports = function(deployer) {
  deployer.deploy(FotoToken, 100 * 10 ** 8, 'FotoToken', 8, 'FOTO')
}
